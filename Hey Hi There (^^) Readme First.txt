just a simple test to prove my skills in development by making a bomberman replica using unity alone without any external help besides the unity docs
so let's get started :)

I will always complete my taks by having small goals like checkpoints and piece all of them together to reach my goal
so i will note my checkpoints to reach in steps wise

Note: making all the customizations as scriptable objects so that it makes the game customisation easier and faster for the designers without any knowledge of development
        and also makes the game good for testing in isolation situations without raising nul refernce exceptions reducing the risk of loosing refernces in the editor
         (when working in a bigger scale with bigger team) modifing and saving values on runtime 
         
         Setting file can be created under "Create/Sajid/GameSettings"

Step-1: making basic player movements 
        [
            A player that detects walls and stops exactly before hitting the wall so that the player will have seamles movement with a single unit snapped per 
            
            Method of achiving :
                - Raycast from the player towards all the sides and check if it hits an object tagged Either "Enviornment or Destroyable" if does then stop movement
                    (we have an option to make snappy movement or a smooth movement flow)
                    
            Movement keys "WASD"
        ]
 
Step-2: making the bomb that destroys only destroyable objects 
        [
            When the player drops the bomb by pressing space and should destroy objects around it in linear explosion 
            
            Method of achiving :
                Create a script that controls the explosion by behaving in such a way that whenever an object with that script touches another object tagged "Destroyable" 
                it destroys the other object and destroy itself as well to get a linear explosion make a bomb in a "+" shape and extrude it's edeges to increase the range
                 (flexible enough to further add more features and extend the range as we like)
                
            Bomb key "Space"    
                
        ]
        
Step-3: Making second player
        [
            it works by reusing the same player controller script but changing the parameters depending on which object it is attached to and scriptable objects can be 
            a issue in this but a small workaround will work fine
            
            Player-1 Blue Player: WASD for movements and space to place a bomb
            Player-2: Green player: Up,Down,Left,Right arrow keys for movements and rightshift for placing bombs
        ]
Step-4: Powerups
        [
            using scriptable objects really work well when it comes highly accessible data and implimenting powerups is really easy because of that 
            as being aproject manager and lead developer in my present company my hands are too full with other stuff and coudn't spend much time on them
        ]
        
Total Time Taken:
        [
            Two hours a day(Saturday,Sunday&Monday) a total of 6 hours with implimentation of core game functionalities debuggable and testable with expandable gameplay
            system and mechanisim for a fluent flow in development process 
            it might be a little buggy or not highly optmised at certain places and does the basic functionality 
        ]
The future of the project:
        [
            the project is build in such a way that even the designers with no knowledge of development can understand, modify and make gameplay improvements
            and also it makes new developers easier to understand the system and build upon it
            - Makee a proper framework such that it can be reused for a sequence of the game 
            - optmise it further to target the lowest end devices as possible
            - make the system een flexible and fluent for major changes or totally new gameplay progress in 
        ]
        
        it's a really small brief hope it helps in understanding the project and also i was doing development on the go during my break time,cimmute e.t.c.., 
        so it might be pretty messy :P
        
        Thanks for taking your time and checking out the project hope to hear from you soon 
        SAJID (^^)
        Unity Senior Developer
        shaiksajid.in@gmail.com
        www.shaiksajid.xyz