﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public BlueGameSettings BlueGame_Settings;
	public GreenGameSettings GreenGame_Settings;
	Text BlueBombCountText;
	Text GreenBombCountText;
	public Button ReloadButton;

	public GameObject UiPopup;

	void Awake(){
		BlueGame_Settings.GameOver = false;
		GreenGame_Settings.GameOver = false;
		BlueGame_Settings.BombsCount = 1;
		GreenGame_Settings.BombsCount = 1;
	}

	// Use this for initialization
	void Start () {
		ReloadButton.onClick.AddListener (Restart_Game);
		BlueBombCountText = GameObject.FindGameObjectWithTag ("BlueBombCount").GetComponent<Text>();
		GreenBombCountText = GameObject.FindGameObjectWithTag ("GreenBombCount").GetComponent<Text>();
		BlueGame_Settings.BombsCount = 1;
		GreenGame_Settings.BombsCount = 1;
	}
	
	// Update is called once per frame
	void Update () {		
		if(BlueGame_Settings.BombsCount >=1){
			BlueBombCountText.text = "1";		
		}
		if(BlueGame_Settings.BombsCount <1){
			BlueBombCountText.text = "0";		
		}
		if(GreenGame_Settings.BombsCount >=1){			
			GreenBombCountText.text = "1";
		}
		if(GreenGame_Settings.BombsCount <1){			
			GreenBombCountText.text = "0";
		}

		if (BlueGame_Settings.BombsCount*5 < 5) {
			BlueGame_Settings.BombLimit = true;
		} else {
			BlueGame_Settings.BombLimit = false;
		}

		if (GreenGame_Settings.BombsCount*5 < 5) {
			GreenGame_Settings.BombLimit = true;
		} else {
			GreenGame_Settings.BombLimit = false;
		}

		if(BlueGame_Settings.GameOver){		
			UiPopup.SetActive (true);
			UiPopup.GetComponentInChildren<Text> ().text = "GreenPlayerWon";
			BlueGame_Settings.GameOver = true;
		}
		if(GreenGame_Settings.GameOver){
			UiPopup.SetActive (true);
			UiPopup.GetComponentInChildren<Text> ().text = "BluePLayerWon";	
			GreenGame_Settings.GameOver = true;
		}
	}

	void Restart_Game(){
		SceneManager.LoadScene ("Game", LoadSceneMode.Single);
	}
}
