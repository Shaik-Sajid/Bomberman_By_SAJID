﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombManager : MonoBehaviour {

	public BlueGameSettings BlueBomb_Manager;
	public GreenGameSettings GreenBomb_Manager;
	public bool GreenPlayer;

	void Awake(){
		GetComponent<Collider> ().enabled = false;
	}

	void Start () {								
		if(GreenPlayer){
			GreenBomb_Manager.BombsCount-=1;	
		}else{
			BlueBomb_Manager.BombsCount-=1;	
		}
		StartCoroutine (DestryObjects ());
	}

	IEnumerator DestryObjects(){						
		yield return new WaitForSeconds (BlueBomb_Manager.ExplosionTime);
		GetComponent<Collider> ().enabled = true;
		if (GreenPlayer) {
			GreenBomb_Manager.BombsCount++;	
		} else {
			BlueBomb_Manager.BombsCount++;	
		}
		yield return new WaitForSeconds (0.1f);
		Destroy (gameObject);
	}		


	void OnTriggerEnter(Collider Col_Obj) {		
		if (Col_Obj.gameObject.tag == "Destroyable" || Col_Obj.gameObject.tag == "Enemy") {
			Destroy (Col_Obj.gameObject);
		}

		if (Col_Obj.gameObject.tag == "BluePlayer") {
			BlueBomb_Manager.GameOver = true;
		}
		if (Col_Obj.gameObject.tag == "GreenPlayer") {
			GreenBomb_Manager.GameOver = true;
		}
	}
}
