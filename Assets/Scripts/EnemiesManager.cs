﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesManager : MonoBehaviour {

	public float MinDistance=2.0f;
	public float MaxDistance=3.0f;
	public float Speed = 2.0f;

	public bool MoveVertical;

	void Update () {
		if(MoveVertical)
			transform.position =new Vector3(transform.position.x, transform.position.y, Mathf.PingPong(Time.time*Speed,MaxDistance-MinDistance)+MinDistance);
		else
			transform.position =new Vector3(Mathf.PingPong(Time.time*Speed,MaxDistance-MinDistance)+MinDistance, transform.position.y, transform.position.z);
	}
}
