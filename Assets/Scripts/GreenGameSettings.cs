﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "GreenGameSettings", menuName = "SAJID/GreenGameSettings", order = 1)]
public class GreenGameSettings : ScriptableObject {   
	[Header("Demo game of Bomberman Created by SHAIK SAJID (^^)")]
	[Space]
	[Header("Player Settings")]
	[Space]
	public float Player_Speed = 1.0f;
	public float PlayerLimit = 0.5f;
	public bool BombLimit = false; 
	[Space]
	[Header("Bomb Settings")]
	[Space]
	public GameObject PlayerBomb;
	public float ExplosionTime = 3.0f;
	[Space]
	[Header("Game Settings")]
	[Space]
	public int BombsCount;
	[Space]
	[Header("Game Over")]
	[Space]
	public bool GameOver;
}