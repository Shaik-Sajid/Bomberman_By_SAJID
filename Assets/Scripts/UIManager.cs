﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

	public Button StartGame;

	public BlueGameSettings Blue_Game_Settings;
	public GreenGameSettings Green_Game_Settings;

	// Use this for initialization
	void Start () {
		StartGame.onClick.AddListener (Start_Game);
		Blue_Game_Settings.GameOver = false;
		Green_Game_Settings.GameOver = false;
		Blue_Game_Settings.BombsCount = 1;
		Green_Game_Settings.BombsCount = 1;
	}

	public void Start_Game(){
		SceneManager.LoadScene ("Game", LoadSceneMode.Single);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
