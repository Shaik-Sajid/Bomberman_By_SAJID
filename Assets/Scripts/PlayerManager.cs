﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	public BlueGameSettings BluePlayer_Manager;
	public  GreenGameSettings GreenPlayer_Manager;
	public bool BluePlayer = true;
	RaycastHit objectHit;


	// Use this for initialization
	void Start () {
		
	}		

	// Update is called once per frame
	void Update () {			
		if(BluePlayer){			
			//Initiate ray ditrection
			Vector3 Forward = transform.TransformDirection(Vector3.forward);
			Vector3 Backward = transform.TransformDirection(Vector3.back);
			Vector3 LeftSide = transform.TransformDirection(Vector3.left);
			Vector3 RightSide = transform.TransformDirection(Vector3.right);

			//ForDebugPurpose
			Debug.DrawRay(transform.position, Forward * BluePlayer_Manager.PlayerLimit, Color.red);
			Debug.DrawRay(transform.position, Backward * BluePlayer_Manager.PlayerLimit, Color.green);
			Debug.DrawRay(transform.position, LeftSide * BluePlayer_Manager.PlayerLimit, Color.green);
			Debug.DrawRay(transform.position, RightSide * BluePlayer_Manager.PlayerLimit, Color.green);
			//Player movement conditions
			if (Input.GetKeyUp (KeyCode.W)) {				
				if (Physics.Raycast (transform.position, Forward, out objectHit, BluePlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate (0, 0,BluePlayer_Manager.Player_Speed);	
					}
				} else {
					transform.Translate (0, 0, BluePlayer_Manager.Player_Speed);	
				}

			}
			if (Input.GetKeyUp (KeyCode.S)) {
				if (Physics.Raycast (transform.position, Backward, out objectHit, BluePlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate (0, 0, -BluePlayer_Manager.Player_Speed);	
					}
				} else {
					transform.Translate (0, 0, -BluePlayer_Manager.Player_Speed);	
				}

			}
			if (Input.GetKeyUp (KeyCode.A)) {
				if (Physics.Raycast (transform.position, LeftSide, out objectHit, BluePlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate (-BluePlayer_Manager.Player_Speed, 0,0);	
					}
				} else {
					transform.Translate ( -BluePlayer_Manager.Player_Speed, 0,0);	
				}

			}
			if (Input.GetKeyUp (KeyCode.D)) {
				if (Physics.Raycast (transform.position, RightSide, out objectHit, BluePlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate ( BluePlayer_Manager.Player_Speed, 0,0);	
					}
				} else {
					transform.Translate ( BluePlayer_Manager.Player_Speed, 0,0);	
				}
			}

			//Spawn a bomb on space pressed
			if(Input.GetKeyUp(KeyCode.Space)){
				if(!BluePlayer_Manager.BombLimit){				
					GameObject Bombtype = Instantiate(BluePlayer_Manager.PlayerBomb, transform.position, Quaternion.identity);
					Bombtype.GetComponent<BombManager> ().GreenPlayer = false;
				}
			}				
		}

		if(gameObject.tag == "GreenPlayer" && !BluePlayer){
			//Initiate ray ditrection
			Vector3 Forward = transform.TransformDirection(Vector3.forward);
			Vector3 Backward = transform.TransformDirection(Vector3.back);
			Vector3 LeftSide = transform.TransformDirection(Vector3.left);
			Vector3 RightSide = transform.TransformDirection(Vector3.right);

			//ForDebugPurpose
			Debug.DrawRay(transform.position, Forward * GreenPlayer_Manager.PlayerLimit, Color.red);
			Debug.DrawRay(transform.position, Backward * GreenPlayer_Manager.PlayerLimit, Color.green);
			Debug.DrawRay(transform.position, LeftSide * GreenPlayer_Manager.PlayerLimit, Color.green);
			Debug.DrawRay(transform.position, RightSide * GreenPlayer_Manager.PlayerLimit, Color.green);

			//Player movement conditions
			if (Input.GetKeyUp (KeyCode.UpArrow)) {
				if (Physics.Raycast (transform.position, Forward, out objectHit, GreenPlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate (0, 0,GreenPlayer_Manager.Player_Speed);	
					}
				} else {
					transform.Translate (0, 0, GreenPlayer_Manager.Player_Speed);	
				}

			}
			if (Input.GetKeyUp (KeyCode.DownArrow)) {
				if (Physics.Raycast (transform.position, Backward, out objectHit, GreenPlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate (0, 0, -GreenPlayer_Manager.Player_Speed);	
					}
				} else {
					transform.Translate (0, 0, -GreenPlayer_Manager.Player_Speed);	
				}

			}
			if (Input.GetKeyUp (KeyCode.LeftArrow)) {
				if (Physics.Raycast (transform.position, LeftSide, out objectHit, GreenPlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate (-GreenPlayer_Manager.Player_Speed, 0,0);	
					}
				} else {
					transform.Translate ( -GreenPlayer_Manager.Player_Speed, 0,0);	
				}

			}
			if (Input.GetKeyUp (KeyCode.RightArrow)) {
				if (Physics.Raycast (transform.position, RightSide, out objectHit, GreenPlayer_Manager.PlayerLimit)) {					
					if(objectHit.collider.tag == "Enviorment" || objectHit.collider.tag == "Destroyable"){
						return;
					}else{
						transform.Translate ( GreenPlayer_Manager.Player_Speed, 0,0);	
					}
				} else {
					transform.Translate ( GreenPlayer_Manager.Player_Speed, 0,0);	
				}
			}

			//Spawn a bomb on space pressed
			if(Input.GetKeyUp(KeyCode.RightShift)){
				if(!GreenPlayer_Manager.BombLimit){				
					GameObject Bombtype = Instantiate(GreenPlayer_Manager.PlayerBomb, transform.position, Quaternion.identity);
					Bombtype.GetComponent<BombManager> ().GreenPlayer = true;
				}
			}				
		}
	}

	void OnTriggerEnter(Collider Col_Obj) {		
		if (Col_Obj.tag == "Enemy") {			
			if (gameObject.tag == "GreenPlayer") {
				GreenPlayer_Manager.GameOver = true;
			} else {
				BluePlayer_Manager.GameOver = true;
			}				
		}
	}
}
